const fs = require("fs");
const Hotel = require("./Hotel");
const Command = require("./Command");

function main() {
  const filename = "input.txt";
  const commands = getCommandsfromFileName(filename);

  commands.forEach((command) => {
    switch (command.name) {
      case "create_hotel": {
        const [floor, roomPerFloor] = command.params;
        this.hotel = new Hotel(floor, roomPerFloor);
        console.log(
          `Hotel created with ${floor} floor(s), ${roomPerFloor} room(s) per floor.`
        );
        break;
      }

      case "book": {
        const [roomNumber, name, age] = command.params;
        hotel.checkIn(roomNumber, name, age);
        break;
      }

      case "list_available_rooms": {
        const availableRooms = hotel.getAvailableRooms();
        console.log(availableRooms);
        break;
      }

      case "checkout": {
        const [keyCardNumber, checkOutName] = command.params;
        this.hotel.checkOut(keyCardNumber, checkOutName);
        break;
      }

      case "list_guest": {
        const guests = this.hotel.getGuests();
        console.log(guests);
        break;
      }

      case "get_guest_in_room": {
        const [roomNumber] = command.params;
        const guest = this.hotel.getGuestInTheRoom(roomNumber);
        console.log(guest);
        break;
      }

      case "list_guest_by_age": {
        const [sign, guestAge] = command.params;
        const guests = this.hotel.getGuestsByAge(sign, guestAge);
        console.log(guests);
        break; 
      }

      case "list_guest_by_floor": {
        const [floor] = command.params;
        const guests = this.hotel.getGuestsByFloor(floor);
        console.log(guests);
        break;
      }

      case "checkout_guest_by_floor": {
        const [floor] = command.params;
        const guests = this.hotel.checkOutGuestsByFloor(floor);
        break;
      }

      case "book_by_floor": {
        const [floor, name, age] = command.params;
        this.hotel.bookByFloor(floor, name, age);
        break;
      }

      default:
        break;
    }
  });
}

function getCommandsfromFileName(fileName) {
  const file = fs.readFileSync(fileName, "utf-8");

  return file
    .split("\n")
    .map((line) => line.split(" "))
    .map(
      ([commandName, ...params]) =>
        new Command(
          commandName,
          params.map((param) => {
            const parsedParam = parseInt(param, 10);

            return Number.isNaN(parsedParam) ? param : parsedParam;
          })
        )
    );
}

main();
