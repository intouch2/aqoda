class Room {
    constructor(roomNumber) {
      this.roomNumber = roomNumber;
      this.guestName = ""
      this.guestAge = ""
      this.keyCard = 0; // keyCard = 0 means that there is no keyCard
      this.isBooked = false;
    }
    
    isAvailable() {
      return !(this.isBooked);
    }

    checkInGuest(name, age, CardToBeGiven) {
      CardToBeGiven.useCard();
      this.guestName = name;
      this.guestAge = age;
      this.keyCard = CardToBeGiven;
      this.isBooked = true;
    }

    checkOutGuest() {
      this.keyCard.returnCard();
      this.guestName = "";
      this.guestAge = "";
      this.keyCard = 0;
      this.isBooked = false;
    }
}

module.exports = Room;