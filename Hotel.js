const Room = require("./Room");
const KeyCard = require("./KeyCard");

class Hotel {
    constructor(NumOfFloor, NumOfRoomsPerFloor) {
      this.RoomList = this.createRoomList(NumOfFloor, NumOfRoomsPerFloor);
      this.KeyCardList = this.createKeyCardList(NumOfFloor * NumOfRoomsPerFloor);
    }

    createKeyCardList(TotalNumberOfRooms) {
        let keyCardList = [];

        for (let keycardnum = 1; keycardnum <= TotalNumberOfRooms; ++keycardnum) {
          let newKeyCard = new KeyCard(keycardnum, false);
          keyCardList.push(newKeyCard);
        }

        return keyCardList;
    }
      
    createRoomList(NumOfFloor, NumOfRoomsPerFloor) {
        let RoomList = [];

        for (let FloorNumber = 1; FloorNumber <= NumOfFloor; ++FloorNumber) {
          for (let RoomNumber = 1; RoomNumber <= NumOfRoomsPerFloor; ++RoomNumber) {
            let newRoom = new Room(FloorNumber * 100 + RoomNumber);
            RoomList.push(newRoom);
          }
        }

        return RoomList;
    }

    checkIn(roomnumber, name, age) {
      const room = this.getRoomByRoomNumber(roomnumber);

      if (room.isAvailable()) {
        const CardToBeGiven = this.findCardToBeGiven();

        room.checkInGuest(name, age, CardToBeGiven);

        console.log(
          `Room ${roomnumber} is booked by ${name} with Keycard number ${room.keyCard.getKeyCardNumber()}`
        ); 
      } 
      else {
        console.log(
          `Cannot book room ${roomnumber} for ${name}, The room is currently booked by ${
            room.guestName}.`
        );
      }
    }
  
    checkOut(keyCardId, name) {
      const room = this.getRoomWithGuestNameAndKeyCardId(name, keyCardId);
  
      if (!room) {
        const cardOwner = this.getCardOwner(keyCardId);

        console.log(
          `Only ${cardOwner} can checkout with key card number ${keyCardId}.`
        );
  
        return;
      }
  
      room.checkOutGuest();
  
      console.log(`Room ${room.roomNumber} is checkout.`);
    }

    getAvailableRooms(){
      const availableRoomList = this.RoomList.filter(room => !room.isBooked).map(room => room.roomNumber);
      return availableRoomList.join(' ');
    }

    getGuestsByAge(sign, age) {
        const guestList = this.RoomList.filter((room) => room.isBooked)
          .filter((room) => {
            switch (sign) {
              case "<":
                return room.guestAge < age;
  
              case "=":
                return room.guestAge === age;

              case ">":
                return room.guestAge > age;
            }
          })
          .map((room) => room.guestName);
    
        return guestList.join(', ');
    }    

    getGuests() {
      let guestList = new Set();

      this.RoomList.filter((room) => {
        return room.isBooked;
      }).forEach((room) => {
        guestList.add(room.guestName);
      });

      return (Array.from(guestList).join(", "));
    }
  
    getCardOwner(keyCardId) {
      const room = this.RoomList.find((room) => room.keyCard.KeyCardNumber === keyCardId);
      return room.guestName;
    }

    findCardToBeGiven() {
      const card = this.KeyCardList.find((card) => card.isGiven === false);
      return card;
    }

    getRoomByRoomNumber(roomNumber) {
      const room = this.RoomList.find((room) => room.roomNumber === roomNumber);
      return room;
    }

    getGuestInTheRoom(roomNumber) {
      const room = this.RoomList.find((room) => room.roomNumber === roomNumber);
      return room.guestName;
    }

    getRoomWithGuestNameAndKeyCardId(name, id) {
      return this.RoomList.find(
        (room) => room.guestName === name && room.keyCard.KeyCardNumber === id
      );
    }

    getGuestsByFloor(floor){
      let guestList = new Set();

      this.RoomList.filter(room =>{
          return (Math.floor(room.roomNumber/100) === floor && room.isBooked);
      }).forEach(roomOnFloor => {
          guestList.add(roomOnFloor.guestName);
      });

      guestList = Array.from(guestList);

      return guestList.join(', ');
    }

    checkOutGuestsByFloor(floor){
      let roomNumberList = [];

      this.RoomList.filter(room => {
        return (Math.floor(room.roomNumber/100) === floor && room.isBooked);
      }).forEach(room => {
        roomNumberList.push(room.roomNumber);
        room.checkOutGuest();
      });

      roomNumberList = roomNumberList.join(', ');

      console.log(`Room ${roomNumberList} are checkout.`);
    }

    bookByFloor(floor, guestName, guestAge){
      let roomListByFloor = this.RoomList.filter(room => {
        return Math.floor(room.roomNumber/100) === floor;
      });

      const checkForBookedRoom = roomListByFloor.find(room => room.isBooked);

      if (checkForBookedRoom != undefined){
        console.log(`Cannot book floor ${floor} for ${guestName}.`);
        return;
      }

      let roomNumberList = [];
      let roomKeyCardList = [];

      roomListByFloor.forEach(room => {
        const CardToBeGiven = this.findCardToBeGiven();
        room.checkInGuest(guestName, guestAge, CardToBeGiven);
        roomNumberList.push(room.roomNumber);
        roomKeyCardList.push(room.keyCard.getKeyCardNumber());
      });

      console.log(`Room ${roomNumberList.join(', ')} are booked with keycard number ${roomKeyCardList.join(', ')}.`);
    }
}

module.exports = Hotel;