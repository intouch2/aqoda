class KeyCard {
    constructor(KeyCardNumber, isGiven) {
      this.KeyCardNumber = KeyCardNumber;
      this.isGiven = isGiven;
    }

    getKeyCardNumber() {
      return this.KeyCardNumber;
    }

    useCard() {
      this.isGiven = true;
    }

    returnCard() {
      this.isGiven = false;
    }
}

module.exports = KeyCard;